package com.example.davaleban5

import android.icu.text.CaseMap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var registerEmail: EditText
    private lateinit var registerButton: Button
    private lateinit var registerPassword : EditText
    private lateinit var registerPassword2 : EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        registerListeners()

    }

    private fun init(){
        registerEmail = findViewById(R.id.registerEmail)
        registerButton = findViewById(R.id.registerButton)
        registerPassword = findViewById(R.id.registerPassword)
        registerPassword2 = findViewById(R.id.registerPassword2)
    }

    private fun registerListeners(){

        registerButton.setOnClickListener(){

            val email = registerEmail.text.toString()
            val password = registerPassword.text.toString()
            val password2 = registerPassword2.text.toString()

            if (email.isEmpty() || password.isEmpty()){
                Toast.makeText(this, "ველები ცარიელია! , ცადეთ ხელახლა", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.length < 9 || password != password2 || password.length < 9 || password2.length < 9){
                Toast.makeText(this, "პაროლი უნდა შეიცავდეს მინიმუმ 9 სიმბოლოს", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener { result ->
                    if(result.isSuccessful){
                        Toast.makeText(this, "რეგისტრაცია წარმატებით გაიარეთ!", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "რეგისტრაცია ვერ მოხერხდა , ცადეთ ხელახლა", Toast.LENGTH_SHORT).show()
                    }

                }

        }
    }
}

